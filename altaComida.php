<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ticket</title>
    <link rel="stylesheet" type="text/css" href="styletk.css">
    <link rel="icon" href="img/onigiri.png">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            max-width: 300px;
            margin: 0 auto;
            font-family: Arial, sans-serif;
        }

        h2{
        color: black;
        font-size: 3em;
        text-align: center; 
        }

        th, td {
            padding: 4px;
        }

        th {
            background-color: #f2f2f2;
            text-align: left;
            font-weight: bold;
        }

        td {
            border-bottom: 1px dashed #ccc;
        }

        tr:last-child td {
            border-bottom: none;
        }

        tfoot th:first-child {
            text-align: right;
        }

        tfoot td:first-child {
            text-align: center;
        }

        tfoot td:last-child {
            font-weight: bold;
        }
    </style>
</head>
<body>
    
</body>
</html>

<?php
if (isset($_POST['ok'])){
    require_once('./Clases/comida.class.php');


    $oni = $_POST['oni'];
    $maki = $_POST['maki'];
    $rmn = $_POST['rmn'];
    $yaki = $_POST['yaki'];
    $kum = $_POST['kum'];


    $mat = $_POST['mat'];
    $fnt = $_POST['fnt'];
    $mune = $_POST['mune'];
    $ske = $_POST['ske'];
    $hpu = $_POST['hpu'];

 
    $oni_price = 45;
    $maki_price = 50;
    $rmn_price = 30;
    $yaki_price = 60;
    $kum_price = 50;


    $mat_price = 20;
    $fnt_price = 20;
    $mune_price = 20;
    $ske_price = 25;
    $hpu_price = 20;

    
    $total_cost = ($oni * $oni_price) + ($maki * $maki_price) + ($rmn * $rmn_price) + ($yaki * $yaki_price) + ($kum * $kum_price) +
        ($mat * $mat_price) + ($fnt * $fnt_price) + ($mune * $mune_price) + ($ske * $ske_price) + ($hpu * $hpu_price);

    
    $comi1 = new Comida($oni, $maki, $rmn, $yaki, $kum, $mat, $fnt, $mune, $ske, $hpu);

    
    $comi1->price_list =[
        'oni' => $oni_price,
        'maki' => $maki_price,
        'rmn' => $rmn_price,
        'yaki' => $yaki_price,
        'kum' => $kum_price,
        'mat' => $mat_price,
        'fnt' => $fnt_price,
        'mune' => $mune_price,
        'ske' => $ske_price,
        'hpu' => $hpu_price
    ];
    $comi1->cost = $total_cost;


    $comidajson = json_encode($comi1, JSON_PRETTY_PRINT);


    $archivo = __DIR__ . "/comida.json";
    if (!file_exists($archivo)) {
        $file = fopen($archivo, "w");
        fwrite($file, "[\n");
    } else {
        $file = fopen($archivo, "c");
        fseek($file, -2, SEEK_END);
        fwrite($file, ",\n");
    }

    fwrite($file, $comidajson);
    fwrite($file, "\n]");
    fclose($file);

echo "<script>alert('Thank you for your purchase 🫰 Your ticket will be generated');</script>";
header("refresh:0; url='menu.php'");
} else {
    echo "<script>alert('Direct Access is not permitted on this page');</script>";
    header("refresh:15; url='menu.html'");
}
