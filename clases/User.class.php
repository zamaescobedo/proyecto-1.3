<?php

    class User implements JsonSerializable{

    	private $telefono;
    	private $contrasena;


    	function __construct($tl, $pass){
    		$this -> telefono = $tl;
    		$this -> contrasena = $pass;
    	}

        function getTelefono() {
            return $this->telefono;
        }

        function getPass() {
            return $this->contrasena;
        }

        
        function setTelefono($tele) {
            $this->telefono = $tele;
          }

        function setPass($pas) {
            $this->contrasena = $pas;
          }

        function jsonSerialize(){
            return get_object_vars($this);
        }

    }


?>