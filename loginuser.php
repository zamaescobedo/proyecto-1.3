<?php

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    require_once('./clases/User.class.php');

    $telefono = $_POST['tel'];
    $contrasena = $_POST['pass'];

    $regex_contraseña = '/^[a-zA-Z0-9]{8,}$/';
    $regex_telefono = '/^[1-9]{2}[0-9]{8}$/';

    if (!preg_match($regex_telefono, $telefono)) {
        echo ("El número de teléfono no es válido");
        header("refresh:2; url='index.html'");
        exit();
    }

    if (!preg_match($regex_contraseña, $contrasena)) {
        echo ("La contraseña debe tener al menos 8 caracteres y NO se aceptan simbolos");
        header("refresh:2; url='index.html'");
        exit();
    }

    if (file_exists('users.json')) {
        $usuarios = json_decode(file_get_contents('users.json'), true);
    } else {
        $usuarios = array();
    }

    foreach ($usuarios as $usuario) {
        if ($usuario["telefono"] == $telefono) {
            echo ("<script>alert('The phone number you entered is already registered');</script>");
            header("refresh:0; url='login.html'");
            exit();
        }
    }

    $user1 = new User($telefono, $contrasena);
    $usuarios[] = array("telefono" => $user1->getTelefono(), "password" => $user1->getPass());

    file_put_contents('users.json', json_encode($usuarios, JSON_PRETTY_PRINT));

    $admin = new User("8713956170","1234567q");

    $userfound = false;

    foreach ($usuarios as $usuario){
        if ($usuario["telefono"] == $telefono && $usuario["password"] == $contrasena) {
            $userfound = true;
            break;
        }
    }

    foreach ($usuarios as $usuario) {
        if ($usuario["telefono"] == $telefono && $usuario["password"] == $contrasena) {
            $userfound = true;
            break;
        }
    }

    if (!$userfound) {
        echo ("Información incorrecta, por favor intente de nuevo");
        header("refresh:2; url='./register.html'");
    } else {
        header("refresh:0; url='login.html'");
    }
}

?>