<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ticket</title>
    <link rel="stylesheet" type="text/css" href="styletk.css">
    <link rel="icon" href="img/onigiri.png">
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
            max-width: 300px;
            margin: 0 auto;
            font-family: Arial, sans-serif;
        }

        h2 {
            color: black;
            font-size: 3em;
            text-align: center;
        }
        button {
           width: 5%;
           height: 40px;
           border-radius: 20px;
           background: grey;
           position: absolute;
           left: 50%;
           top: 55%;
           transform: translate(-50%, -50%);
        }

        th, td {
            padding: 4px;
        }

        th {
            background-color: #f2f2f2;
            text-align: left;
            font-weight: bold;
        }

        td {
            border-bottom: 1px dashed #ccc;
        }

        tr:last-child td {
            border-bottom: none;
        }

        tfoot th:first-child {
            text-align: right;
        }

        tfoot td:first-child {
            text-align: center;
        }

        tfoot td:last-child {
            font-weight: bold;
        }
    </style>
</head>
<body>
<h2>Receipt</h2>

<?php

$json_data = file_get_contents('comida.json');

$comida_array = json_decode($json_data, true);

$ultimo = count($comida_array);
$comida = $comida_array[$ultimo -1];

$oni = $comida['Onigiri'];
$maki = $comida['Maki_Sushi'];
$rmn = $comida['Ramen'];
$yaki = $comida['Takoyaki'];
$kum = $comida['Nikuman'];
$mat = $comida['Te_matcha'];
$fnt = $comida['Fanta'];
$mune = $comida['Ramune'];
$ske = $comida['Sake'];
$hpu = $comida['Happoshu'];
$oni_price = $comida['price_list']['oni'];
$maki_price = $comida['price_list']['maki'];
$rmn_price = $comida['price_list']['rmn'];
$yaki_price = $comida['price_list']['yaki'];
$kum_price = $comida['price_list']['kum'];
$mat_price = $comida['price_list']['mat'];
$fnt_price = $comida['price_list']['fnt'];
$mune_price = $comida['price_list']['mune'];
$ske_price = $comida['price_list']['ske'];
$hpu_price = $comida['price_list']['hpu'];
$total_cost = $comida['cost'];

echo "<table>";
echo "<tr><th>Item</th><th>Quantity</th><th>Price</th><th>Subtotal</th></tr>";
echo "<tr><td>Oni</td><td>$oni</td><td>$oni_price</td><td>" . ($oni * $oni_price) . "</td></tr>";
echo "<tr><td>Maki</td><td>$maki</td><td>$maki_price</td><td>" . ($maki * $maki_price) . "</td></tr>";
echo "<tr><td>Rmn</td><td>$rmn</td><td>$rmn_price</td><td>" . ($rmn * $rmn_price) . "</td></tr>";
echo "<tr><td>Yaki</td><td>$yaki</td><td>$yaki_price</td><td>" . ($yaki * $yaki_price) . "</td></tr>";
echo "<tr><td>Kum</td><td>$kum</td><td>$kum_price</td><td>" . ($kum * $kum_price) . "</td></tr>";
echo "<tr><td>Mat</td><td>$mat</td><td>$mat_price</td><td>" . ($mat * $mat_price) . "</td></tr>";
echo "<tr><td>Fnt</td><td>$fnt</td><td>$fnt_price</td><td>" . ($fnt * $fnt_price) . "</td></tr>";
echo "<tr><td>Mune</td><td>$mune</td><td>$mune_price</td><td>" . ($mune * $mune_price) . "</td></tr>";
echo "<tr><td>Ske</td><td>$ske</td><td>$ske_price</td><td>" . ($ske * $ske_price) . "</td></tr>";
echo "<tr><td>Hpu</td><td>$hpu</td><td>$hpu_price</td><td>" . ($hpu * $hpu_price) . "</td></tr>";
echo "<tr><td colspan='3'>Total Cost</td><td>$total_cost</td></tr>";
echo "</table>";

?>
<button onclick="redirectTopage()">Next</button>

<script>
        function redirectTopage() {
            window.location.href = "menu.html"
        }
</script>
