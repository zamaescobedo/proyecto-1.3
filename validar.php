<?php

$usuarios = json_decode(file_get_contents('users.json'), true);

$telefono = $_POST['tel'];
$contrasena = $_POST['pass'];

$usuarioEncontrado = false;

foreach ($usuarios as $usuario) {
    if ($usuario['telefono'] == $telefono && $usuario['password'] == $contrasena) {
        $usuarioEncontrado = true;
        break;
    }
}

if ($usuarioEncontrado) {
    header("refresh:0; url='menu.html'");
    exit();
} else {
    header("refresh:0; url='login.html'");
    exit();
}


?>